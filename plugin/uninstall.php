<?php
/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/

if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

(function() {
    global $wpdb;

    delete_option( 'foodie_gmaps_api_key' );

    $dbpfx = foodie_prefix();

    $wpdb->query("DROP TABLE {$dbpfx}resources");
    $wpdb->query("DROP TABLE {$dbpfx}questions");
    $wpdb->query("DROP TABLE {$dbpfx}answers");

})();
