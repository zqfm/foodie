<?php
/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/

if (!defined('WPINC')) { die; }

function foodie_prefix () {
    global $wpdb;
    return $wpdb->prefix . FOODIE_PLUGIN_NAME . '_';
}

function foodie_log () {
    if (defined('WP_DEBUG') && WP_DEBUG) {
        call_user_func_array( 'error_log', func_get_args() );
    }
}

function foodie_get_resource_by_id ($id) {
    global $wpdb;

    $dbpfx = foodie_prefix();

    $ret = $wpdb->get_row(
        $wpdb->prepare( "
SELECT
  r.*,
  JSON_ARRAYAGG( JSON_OBJECT( 'id', a.answer_id, 'question', a.question, 'answer', a.answer ) ) AS answers
FROM {$dbpfx}resources r
  LEFT JOIN {$dbpfx}answers a USING (resource_id)
WHERE r.resource_id = %s
GROUP BY r.resource_id
", $id ) );

    if (!empty($ret->answers)) {
        $ret->answers = json_decode( $ret->answers );
    }

    return $ret;
}

function foodie_get_resources ($opts=[]) {
    global $wpdb;

    $dbpfx = foodie_prefix();

    $args = [ FOODIE_RESOURCE_PAGE_SIZE ];

    if (!empty($opts['query'])) {
        $sep = json_decode('"\u001f"'); // unit separator character
        $where = "WHERE CONCAT_WS( '$sep', LOWER(r.title), LOWER(r.address), LOWER(r.contact_name), LOWER(r.contact_email) ) LIKE LOWER(%s)";
        $args[] = '%' . $opts['query'] . '%';
    } else {
        $where = '';
    }

    if (isset($opts['page'])) {
        $limit = 'LIMIT %d, ' . FOODIE_RESOURCE_PAGE_SIZE;
        $offset = (max(intval($opts['page']),1) - 1) * FOODIE_RESOURCE_PAGE_SIZE;
        $args[] = $offset;
    } else {
        $limit = '';
    }

    $query = "
SELECT
  r.*,
  JSON_ARRAYAGG( JSON_OBJECT( 'id', a.answer_id, 'question', a.question, 'answer', a.answer ) ) AS answers,
  CEILING( COUNT(*) OVER () / %d ) page_count,
  COUNT(*) OVER () row_count
FROM {$dbpfx}resources r
  LEFT JOIN {$dbpfx}answers a USING (resource_id)
$where
GROUP BY r.resource_id
ORDER BY r.title
$limit
";

    array_unshift( $args, $query );

    $ret = $wpdb->get_results( call_user_func_array( [ $wpdb, 'prepare' ], $args ) );

    return array_map( function($row) {
        if (!empty($row->answers)) {
            $row->answers = json_decode( $row->answers );
        }
        return $row;
    }, $ret );

}

function foodie_insert_resource ($resource, $question_param) {
    global $wpdb;
    $dbpfx = foodie_prefix();

    $questions = json_decode( $wpdb->get_var( "
SELECT
  JSON_OBJECTAGG(
    question_id,
    JSON_OBJECT( 'question', question, 'type', type ) )
FROM {$dbpfx}questions
" ) );

    foodie_log( '$questions = ' . json_encode( $questions ) );

    $wpdb->insert( "{$dbpfx}resources", $resource );

    $resource_id = $wpdb->insert_id;

    foreach ($questions as $qid => $q) {
        if (isset($question_param[$qid])) {
            $answer = $q->type === 'bool'
                    ? $question_param[$qid] === 'on'
                    : $question_param[$qid];
            $wpdb->insert( "{$dbpfx}answers",
                           [ 'resource_id' => $resource_id,
                             'question' => $q->question,
                             'answer' => $answer ] );
        }
    }

    return $resource;
}

function foodie_update_resource ($id, $resource, $question_param) {
    global $wpdb;
    $dbpfx = foodie_prefix();

    $questions = json_decode( $wpdb->get_var( "
SELECT
  JSON_OBJECTAGG(
    question_id,
    JSON_OBJECT( 'question', question, 'type', type ) )
FROM {$dbpfx}questions
" ) );

    foodie_log( '$questions = ' . json_encode( $questions ) );

    $wpdb->query('START TRANSACTION');

    $wpdb->update( "{$dbpfx}resources", $resource, ['resource_id' => $id] );

    $wpdb->query(
        $wpdb->prepare( "
DELETE FROM ${dbpfx}answers
WHERE resource_id = %d
", $id ) );

    foreach ($questions as $qid => $q) {
        if (isset($question_param[$qid])) {
            $answer = $q->type === 'bool'
                    ? $question_param[$qid] === 'on'
                    : $question_param[$qid];
            $wpdb->insert( "{$dbpfx}answers",
                           [ 'resource_id' => $id,
                             'question' => $q->question,
                             'answer' => $answer ] );
        }
    }

    $wpdb->query('COMMIT');

    return $resource;
}

function foodie_delete_resource ($id) {
    global $wpdb;

    $dbpfx = foodie_prefix();

    $wpdb->query(
        $wpdb->prepare( "
DELETE FROM {$dbpfx}resources
WHERE resource_id = %d
", $id ) );

    return $id;

}
