<?php
/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/

if (!defined('WPINC')) { die; }

if (!defined('FOODIE_VERSION')) {
    define('FOODIE_VERSION', '0.1.0');
}

if (!defined('FOODIE_DB_VERSION')) {
    define('FOODIE_DB_VERSION', '0.1.0');
}

if (!defined('FOODIE_PLUGIN_NAME')) {
    define('FOODIE_PLUGIN_NAME', trim(dirname(dirname(plugin_basename(__FILE__)))));
}

if (!defined('FOODIE_PLUGIN_DIR')) {
    define('FOODIE_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . FOODIE_PLUGIN_NAME);
}

if (!defined('FOODIE_PLUGIN_URL')) {
    define('FOODIE_PLUGIN_URL', WP_PLUGIN_URL . '/' . FOODIE_PLUGIN_NAME);
}

if (!defined('FOODIE_RESOURCE_PAGE_SIZE')) {
    define('FOODIE_RESOURCE_PAGE_SIZE', 10);
}

add_option( FOODIE_PLUGIN_NAME . '_version', FOODIE_VERSION );
