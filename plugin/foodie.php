<?php
/*
Plugin Name: Foodie
Version: 0.1.0
Author: zqfm
License: GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.en.html

Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/

if (!defined('WPINC')) { die; }

$foodie_path = plugin_dir_path( __FILE__ );

require_once "$foodie_path/include/defines.php";
require_once "$foodie_path/include/functions.php";
require_once "$foodie_path/admin/hooks.php";
register_activation_hook( __FILE__, 'foodie_activation_hook' );
register_deactivation_hook( __FILE__, 'foodie_deactivation_hook' );
require_once "$foodie_path/admin/settings.php";
require_once "$foodie_path/public/api.php";
require_once "$foodie_path/public/shortcodes.php";
