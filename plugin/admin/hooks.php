<?php
/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/

if (!defined('WPINC')) { die; }

require_once ABSPATH . 'wp-admin/includes/upgrade.php';

function foodie_create_tables() {
    global $wpdb;

    foodie_log( "foodie_create_tables()" );

    $charset_collate = $wpdb->get_charset_collate();
    $dbpfx = foodie_prefix();

    dbDelta("
CREATE TABLE {$dbpfx}resources (
  resource_id int(11) NOT NULL AUTO_INCREMENT,
  title tinytext NOT NULL,
  lat decimal(10,7),
  lng decimal(10,7),
  address tinytext,
  phone varchar(15),
  url tinytext,
  gmaps_place_id text,
  contact_name text NOT NULL,
  contact_phone varchar(15) NOT NULL,
  contact_email text,
  contact_info text,
  created timestamp NOT NULL DEFAULT NOW(),
  PRIMARY KEY  (resource_id)
) $charset_collate;

CREATE TABLE {$dbpfx}questions (
  question_id int(11) NOT NULL AUTO_INCREMENT,
  question tinytext NOT NULL,
  type tinytext NOT NULL,
  description tinytext,
  required BOOL NOT NULL DEFAULT false,
  options tinytext,
  PRIMARY KEY  (question_id)
) $charset_collate;

CREATE TABLE {$dbpfx}answers (
  answer_id int(11) NOT NULL AUTO_INCREMENT,
  resource_id int(11) NOT NULL REFERENCES {$dbpfx}resources (resource_id),
  question tinytext NOT NULL,
  answer tinytext,
  PRIMARY KEY  (answer_id)
) $charset_collate");

    add_option('foodie_db_version', FOODIE_DB_VERSION);

}

function foodie_init_db() {
    global $wpdb;

    foodie_log( "foodie_init_db()" );
    $dbpfx = foodie_prefix();

    /**
     * NOTE! $wpdb->prepare doesn't create a prepared statement,
     * instead it sanitizes a query string and its parameters
     **/
    $wpdb->query( $wpdb->prepare(
        "
INSERT INTO {$dbpfx}questions (question, type, options)
VALUES ('Resource Type', 'enum', %s),
       ('Hours of Operation', 'text', NULL),
       ('Additional Information', 'text', NULL)",
        json_encode([
            ['type' => 'Food Pantry', 'color' => '#e30f00'],
            ['type' => 'Soup Kitchen', 'color' => '#ffc800'],
            ['type' => "Farmers' Market", 'color' => '#007527'],
            ['type' => 'Senior Meals', 'color' => '#ff6f19'],
            ['type' => 'Church', 'color' => '#006cb7'],
            ['type' => 'Shelter', 'color' => '#880060']
        ]) ) );

}

function foodie_activation_hook() {
    global $wpdb;

    foodie_log( 'foodie_activation_hook()' );

    $db_version = get_option('foodie_db_version');
    foodie_log( "\$db_version: $db_version <=> " . FOODIE_DB_VERSION );

    if (version_compare( $db_version, FOODIE_DB_VERSION, '!=' )) {
        foodie_create_tables();
        update_option('foodie_db_version', FOODIE_DB_VERSION);
    }

    if (!$db_version) {
        foodie_init_db();
    }

}

function foodie_deactivation_hook() {
    foodie_log( 'foodie_deactivation_hook()' );
    unregister_setting( 'foodie', 'foodie_gmaps_api_key' );
}
