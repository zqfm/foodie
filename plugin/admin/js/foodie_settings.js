/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/
(function (self) {

  var foodie = self.foodie;
  var map;

  self.foodieInitMap = function () {
    map = foodie.initMap('.foodie-map');
  };

  jQuery(document).ready( function ($) {

    function changeTypeSelect (e) {
      var $self = $(e.currentTarget);
      if ($self.val() === 'enum') {
        $self.parent().append(
          $('<div>', { class: 'row-actions visible' })
            .append(
              $('<span>').append(
                $('<a>', { href: 'javascript:;', class: 'edit-options' })
                  .on( 'click', editOptionsModal )
                  .text( 'Edit Options' ) ) ) );
      } else {
        $self.siblings('.row-actions,.options').remove();
      }
    }

    function editOptionsModal (e) {

      var tr = $(e.currentTarget).closest('tr');
      var id = tr.data('id');
      var options = tr.data('options') || [];

      var del = $('<button>').text('Delete').on( 'click', function (e) {
        $(e.currentTarget.parentNode).remove();
      } );

      var defoption = id == 1 ? {} : '';

      $('#foodie-options-modal').empty().append(

        $('<div>', { class: 'options' }).append(
          (options.length === 0 ? [defoption] : options).map(
            function (opt) {
              return $('<div>')
                .append( $('<input>', { type: 'text', class: 'option-input' }).val( typeof opt === 'object' ? opt.type || '' : opt ) )
                .append( id == 1 ? $('<input>', { type: 'color', class: 'color-input' }).val( opt.color || '' ) : null )
                .append( del.clone(true) );
            } ) ),

        $('<button>').text('Add').on( 'click', function (e) {
          $(e.currentTarget.parentNode).find('.options').append(
            $('<div>').append(
              $('<input>', { type: 'text', class: 'option-input' }),
              del.clone(true) ) );
        } ),

        $('<button>').text('Save').on( 'click', function (e) {

          var newOptions = Array.from( e.currentTarget.parentNode.querySelectorAll('.option-input') )
              .map( function (input) {
                var color = input.parentNode.querySelector('.color-input');
                if (color) {
                  return { type: input.value.trim(), color: color.value.trim() };
                } else {
                  return input.value.trim();
                }
              } )
              .filter( function (val) { return val; } );

          tr.data( 'options', newOptions );

          tb_remove();

        } ) );

      tb_show( 'Edit Options', '#TB_inline?inlineId=foodie-options-modal' );

    }

    $('.foodie-question select.type').on( 'change', changeTypeSelect );

    $('.foodie-new-question').on( 'click', function (e) {

      var template = document.querySelector('.foodie-question[hidden]');
      var newTr = template.cloneNode( true );
      newTr.removeAttribute('hidden');
      newTr.dataset.id = 'new';

      $(newTr).find('select.type').on( 'change', changeTypeSelect );

      template.parentNode.insertBefore(
        newTr,
        template );

    } );

    function search () {
      var query = $('.foodie-resource-query').val().trim();
      var page = $('.foodie-resource-page').val();
      console.log( 'search', query, page );
      $.post( ajaxurl, {
        action: 'foodie_search_resource',
        _nonce: foodie_settings_data.nonce,
        query: query,
        page: page
      } )
        .then( function (result) {
          if (result.error) {

          } else {
            var rowCount = result.data.length > 0 ? result.data[0].row_count : 0;
            var newMax = result.data.length > 0 ? result.data[0].page_count : 1;
            $('.foodie-resource-first-page, .foodie-resource-prev-page')
              .toggleClass('disabled', page <= 1);
            $('.foodie-resource-next-page, .foodie-resource-last-page')
              .toggleClass('disabled', page >= newMax);
            $('.tablenav-pages').toggleClass('one-page', newMax == 1);
            $('.displaying-num').text( rowCount + ' items' );
            $('.foodie-resource-page').prop('max', newMax);
            $('.foodie-resource-total-pages').text(newMax);
            $('.foodie-resources-table tbody')
              .empty()
              .append(
                result.data.length > 0
                  ? result.data.map(
                    function (resource) {
                      return $('<tr>')
                        .append(
                          $('<td>').append(
                            $('<strong>').text( resource.title ),
                            $('<div>', { class: 'row-actions visible' }).append(
                              $('<span>').append(
                                $('<a>', { href: foodie_settings_data.edit_resource_page + '?foodie-resource=' + encodeURIComponent( resource.resource_id ) })
                                  .text( 'Edit' ) ) ) ),
                          $('<td>').text( resource.contact_name ) );
                    } )
                  : $('<tr>').append( $('<td>', { colspan: 2 }).text( 'No Results') ) );
          }
        } );
    }

    $('.foodie-resource-query')
      .on( 'input', function (e) {
        $('.foodie-resource-page').val( 1 );
      } )
      .on( 'keyup', function (e) {
        if (e.key === 'Enter') {
          e.preventDefault();
          search();
        }
      } );

    $('.foodie-search-resources').on( 'click', search );

    $('.foodie-resource-first-page').on( 'click', function (e) {
      var oldVal = $('.foodie-resource-page').val();
      var newVal = 1;
      $('.foodie-resource-page').val( newVal );
      if (newVal != oldVal) {
        search();
      }
    } );

    $('.foodie-resource-prev-page').on( 'click', function (e) {
      var oldVal = $('.foodie-resource-page').val();
      var newVal = Math.max( (parseInt( oldVal, 10 ) || 0) - 1, 1 );
      $('.foodie-resource-page').val( newVal );
      if (newVal != oldVal) {
        search();
      }
    } );

    $('.foodie-resource-next-page').on( 'click', function (e) {
      var oldVal = $('.foodie-resource-page').val();
      var newVal = Math.min( (parseInt( oldVal, 10 ) || 0) + 1, $('.foodie-resource-page').prop('max') );
      $('.foodie-resource-page').val( newVal );
      if (newVal != oldVal) {
        search();
      }
    } );

    $('.foodie-resource-last-page').on( 'click', function (e) {
      var oldVal = $('.foodie-resource-page').val();
      var newVal = $('.foodie-resource-page').prop('max');
      $('.foodie-resource-page').val( newVal );
      if (newVal != oldVal) {
        search();
      }
    } );

    $('.foodie-resource-page').on( 'input', function(e) {
      var val = parseInt( $(e.currentTarget).val(), 10 );
      if (!isNaN(val) && val >= 1 && val <= $(e.currentTarget).prop('max')) {
        search();
      }
    } );

    $('.foodie-reset-database').on( 'click', function (e) {

      if (confirm( 'This will reset the database to its original state, deleting all data in the process. Are you absolutely certain this is what you want to do?' )) {

        e.currentTarget.disabled = true;
        e.currentTarget.textContent = 'Resetting...';

        $.post( ajaxurl, {
          action: 'foodie_reset_database',
          _nonce: foodie_settings_data.nonce
        } )
          .then( function () {
            e.currentTarget.disabled = false;
            e.currentTarget.textContent = 'Database reset!';
          } );

      }

    } );

    $('.edit-options').on( 'click', editOptionsModal );

    function esc (str) {
      if (/[",\r\n]/.test( str )) {
        return '"' + str.replace(/"/g,'""') + '"';
      } else {
        return str;
      }
    }

    var columns = ['resource_id','title','lat','lng','address','phone','url',
                   'hours','description','place_id','contact_name',
                   'contact_phone','contact_email','contact_info','created'];

    $('.foodie-resource-download').on( 'click', function (e) {

      $.getJSON( foodie_settings_data.api_url + '/foodie/v1/resource/' )
        .done( function (data) {
          if (data.error) {

          } else {

            var questions = {};

            var rows = (data.data || [])
                .map( function (res) {

                  var row = columns
                      .map( function (col) {
                        return esc( res[col] );
                      } )
                      .join(',');

                  var answers = res.answers;

                  Object.keys( questions )
                    .forEach( function (question) {
                      var answerIdx = answers.findIndex( function (ans) {
                        return ans.question === question;
                      } );
                      if (answerIdx > -1) {
                        var answer = answers.splice( answerIdx, 1 )[0];
                        row += ',' + esc( answer.answer );
                      } else {
                        row += ',';
                      }
                    } );

                  answers.forEach( function (answer) {
                    if (answer.question && answer.answer) {
                      questions[ answer.question ] = true;
                      row += ',' + esc( answer.answer );
                    }
                  } );

                  return row;

                } )
                .join('\n');

            var header = columns.map( esc ).join(',') + ','
                + Object.keys( questions ).map( esc ).join(',')
                + '\n';

            var a = document.createElement('a');
            a.style = 'display:none';
            a.href = URL.createObjectURL( new Blob( [header + rows], { type: 'text/csv' } ) );
            a.download = 'resources.csv';
            document.body.appendChild( a );
            a.click();
            a.remove();

          }
        } );

    } );

    function onUpload (e) {

      var $label = $(e.currentTarget).closest('label')
          .text('Uploading...')
          .toggleClass('disabled', true);

      var file = e.currentTarget.files[0];
      file.slice( 0, 132 ).arrayBuffer()
        .then( function (buffer) {

          var string = new TextDecoder().decode( buffer );
          var uploaded_columns = string.split(',').map( function (s) { return s.trim(); } );

          if (columns
              .every( function (col) {
                return uploaded_columns.includes( col ) || col === 'resource_id';
              } ))
          {
            var formData = new FormData();
            formData.append( 'file', file );
            formData.append( 'action', 'foodie_upload_resources' );
            formData.append( '_nonce', foodie_settings_data.nonce );
            $.ajax({
              url: ajaxurl,
              cache: false,
              contentType: false,
              processData: false,
              data: formData,
              type: 'POST',
            }).always( function () {
              $label
                .toggleClass('disabled', false)
                .empty()
                .append(
                  $('<input>', { type: 'file', class: 'foodie-resource-upload', style: 'display:none' })
                    .on( 'change', onUpload ),
                  'Uploaded!' );
            } );
          } else {
            // TODO error message that uploaded file is not valid
          }

        } );

    }

    $('.foodie-resource-upload').on( 'change', onUpload );

    $(document.forms[0]).on( 'submit', function (e) {

      e.preventDefault();

      var questions = Array.from( document.querySelectorAll('.foodie-question') )
          .reduce( function (data, tr) {

            var question = tr.querySelector('.question').value.trim();

            if (question) {

              var row = {
                id: tr.dataset.id,
                question,
                type: tr.querySelector('.type').value,
                required: tr.querySelector('.required').checked
              };

              if (row.type === 'enum') {
                row.options = $(tr).data('options');
              }

              data.push( row );

            }

            return data;

          }, [] );

      var pos = map.getCenter();

      $.post( ajaxurl, {
        action: 'foodie_save_settings',
        _nonce: foodie_settings_data.nonce,
        questions: JSON.stringify( questions ),
        foodie_gmaps_api_key: $('input[name="foodie_gmaps_api_key"]').val(),
        foodie_gmaps_lat: pos.lat(),
        foodie_gmaps_lng: pos.lng(),
        foodie_gmaps_zoom: map.getZoom(),
        foodie_edit_resource_page: $('input[name="foodie_edit_resource_page"]').val(),
        foodie_captcha_site_key: $('input[name="foodie_captcha_site_key"]').val(),
        foodie_captcha_secret_key: $('input[name="foodie_captcha_secret_key"]').val(),
      } )
        .then( function () {

        } );

      return false;


    } );

  });

})(window);
