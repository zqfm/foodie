<?php
/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/

function foodie_init_settings() {

  foodie_log( 'foodie_init_settings()' );

  register_setting( 'foodie', 'foodie_gmaps_api_key' );
  register_setting( 'foodie', 'foodie_edit_resource_page' );
  register_setting( 'foodie', 'foodie_captcha_site_key' );
  register_setting( 'foodie', 'foodie_captcha_secret_key' );

  add_settings_section(
    'foodie_general',
    __('General', 'foodie'),
    '',
    'foodie' );

  add_settings_field(
    'foodie_field_gmaps_key',
    __('Google Maps API Key', 'foodie'),
    'foodie_field_gmaps_key_cb',
    'foodie',
    'foodie_general',
    [] );

  add_settings_field(
    'foodie_field_gmaps_position',
    __('Google Maps Position', 'foodie'),
    'foodie_field_gmaps_position_cb',
    'foodie',
    'foodie_general',
    [] );

  add_settings_field(
    'foodie_edit_resource_page',
    __('Edit Resource Page URL', 'foodie'),
    'foodie_field_edit_resource_page_cb',
    'foodie',
    'foodie_general',
    [] );

  add_settings_field(
    'foodie_captcha_site_key',
    __('reCaptcha Site Key<br><small>Leave blank to disable Captcha</small>', 'foodie'),
    'foodie_captcha_site_key_cb',
    'foodie',
    'foodie_general',
    [] );

  add_settings_field(
    'foodie_captcha_secret_key',
    __('reCaptcha Secret Key', 'foodie'),
    'foodie_captcha_secret_key_cb',
    'foodie',
    'foodie_general',
    [] );

  add_settings_section(
    'foodie_questions',
    __('Questions', 'foodie'),
    'foodie_settings_questions_cb',
    'foodie' );

  add_settings_section(
    'foodie_resources',
    __('Resources', 'foodie'),
    'foodie_settings_resources_cb',
    'foodie' );

  if (WP_DEBUG) {
    add_settings_section(
      'foodie_debug',
      __('Debug', 'foodie'),
      'foodie_settings_debug_cb',
      'foodie' );
  }

}

function foodie_field_gmaps_key_cb() {
  $key = get_option('foodie_gmaps_api_key');
  echo '<input type="text" name="foodie_gmaps_api_key" value="'
     . (isset($key) ? esc_attr($key) : '')
     . '">';
}

function foodie_field_gmaps_position_cb() {
  echo '<div class="foodie-map" style="width:100%;height:400px"></div>';
}


function foodie_field_edit_resource_page_cb() {
    $page = get_option('foodie_edit_resource_page');
    echo '<input type="text" name="foodie_edit_resource_page" value="'
        . (isset($page) ? esc_attr($page) : '')
        . '">';
}

function foodie_captcha_site_key_cb() {
    $key = get_option('foodie_captcha_site_key');
    echo '<input type="text" name="foodie_captcha_site_key" value="'
        . (isset($key) ? esc_attr($key) : '')
        . '">';
}

function foodie_captcha_secret_key_cb() {
    $key = get_option('foodie_captcha_secret_key');
    echo '<input type="text" name="foodie_captcha_secret_key" value="'
        . (isset($key) ? esc_attr($key) : '')
        . '">';
}

function _foodie_question_types($id, $type) {
  $types = [ ['text','Text'],
             ['enum','Select'],
             ['bool','Checkbox'] ];
  return '<select class="type"' . ($id == 1 ? ' disabled' : '') . '>'
       . implode( '', array_map( function($row) use ($type) {
         list($val,$text) = $row;
         $selected = $type === $val ? ' selected' : '';
         return "<option value=\"{$val}\"{$selected}>"
              . __($text, 'foodie')
              . '</option>';
       }, $types ) )
       . '</select>';

}

function foodie_question_row($row, $template=false) {

  $disabled = ($row->question_id ?? '') == 1 ? ' disabled' : '';

  return '<tr class="foodie-question"'
       . ($template ? ' hidden' : '')
       . ' data-id="'
       . ($row->question_id ?? '')
       . '"'
       . (($row->type ?? '') === 'enum'
          ? (' data-options="'
             . htmlspecialchars( $row->options ?? 'null', ENT_QUOTES )
             . '"')
          : '')
       . '>'
       . '<td><input type="text" class="question" value="'
       . htmlspecialchars( $row->question ?? '' )
       . '"' . $disabled . '></td><td>'
       . _foodie_question_types( $row->question_id ?? '', $row->type ?? '' )
       . (($row->type ?? '') === 'enum'
          ? '<div class="row-actions visible">'
          . '<span><a href="javascript:;" class="edit-options">' . __('Edit Options', 'foodie') . '</a></span>'
          . '</div>'
          : '')
       . '</td><td>'
       . '<input class="required" type="checkbox"' . (empty($row->required) ? '' : ' checked') . '></input>'
       . '</td></tr>';

}

function foodie_settings_questions_cb() {
  global $wpdb;

  $dbpfx = foodie_prefix();
  $rows = $wpdb->get_results("SELECT * FROM {$dbpfx}questions");

  echo '<div class="tablenav top">'
      . '<div class="alignleft actions">'
      . '<button type="button" class="foodie-new-question button">New Question</button>'
      . '</div>'
      . '</div>'
      .'<table class="wp-list-table widefat fixed striped foodie-questions-table">'
     . '<thead><tr><th>'
     . __('Question','foodie')
     . '</th><th>'
     . __('Type', 'foodie')
     . '</th><th>'
     . __('Required', 'foodie')
     . '</th>'
     . '</thead>'
     . '<tbody>'
     . implode( '', array_map( 'foodie_question_row', $rows ) )
     . foodie_question_row( new stdClass, true )
     . '</tbody></table>';
}

function foodie_settings_tablenav ($location, $page_count, $row_count) {

    $one_page = $page_count == 1 ? 'one-page' : '';
    $items = sprintf( _n( '%d item', '%d items', $row_count, 'foodie' ), $row_count );
    $label = __('Current Page', 'foodie');
    $disabled = $page_count > 1 ? '' : 'disabled';

    return <<<EOS
<div class="tablenav $location">
  <div class="alignleft actions">
    <button type="button" class="foodie-resource-download button">Download</button>
    <label class="button"><input type="file" class="foodie-resource-upload" style="display:none;"></input>Upload</label>
  </div>
  <div class="tablenav-pages $one_page">
    <span class="displaying-num">$items</span>
    <span class="pagination-links">
     <button type="button" class="foodie-resource-first-page tablenav-pages-navspan button disabled" aria-hidden="true">«</button>
     <button type="button" class="foodie-resource-prev-page tablenav-pages-navspan button disabled" aria-hidden="true">‹</button>
     <span class="paging-input">
       <label for="current-page-selector" class="screen-reader-text">$label</label>
       <input class="foodie-resource-page" type="text" value="1" size="1" max="$page_count" aria-describedby="table-paging">
       <span class="tablenav-paging-text"> of <span class="foodie-resource-total-pages">$page_count</span></span>
     </span>
     <button type="button" class="foodie-resource-next-page tablenav-pages-navspan button $disabled" aria-hidden="true">›</button>
     <button type="button" class="foodie-resource-last-page tablenav-pages-navspan button $disabled" aria-hidden="true">»</button>
   </span>
  </div>
</div>
EOS;
}

function foodie_settings_resources_cb() {

    $resources = foodie_get_resources([ 'page' => 1 ]);

    foodie_log( '$resources = ' . json_encode( $resources ) );

    $page_count = $resources[0]->page_count ?? 1;
    $row_count = $resources[0]->row_count ?? 0;

    $edit_page = get_option('foodie_edit_resource_page');

    echo '<p class="search-box">'
        . '<input type="search" class="foodie-resource-query">'
        . '<button type="button" class="button foodie-search-resources">Search</button>'
        . '</p>'
        . foodie_settings_tablenav( 'top', $page_count, $row_count )
        . '<table class="wp-list-table widefat fixed striped foodie-resources-table">'
        . '<thead><tr><th>'
        . __('Name', 'foodie')
        . '</th><th>'
        . __('Contact', 'foodie')
        . '</th></tr></thead>'
        . '<tbody>'
        . implode( '', array_map( function($resource) use ($edit_page) {
            $edit_url = htmlspecialchars( $edit_page, ENT_QUOTES ) . '?foodie-resource=' . htmlspecialchars( $resource->resource_id, ENT_QUOTES );
            return '<tr data-id="' . $resource->resource_id . '">'
                . '<td>'
                . '<strong>' . $resource->title . '</strong>'
                . '<div class="row-actions visible">'
                . '<span><a href="' . $edit_url .  '" class="edit-resource">' . __('Edit', 'foodie') . '</a></span>'
                . '</div>'
                . '</td><td>'
                . $resource->contact_name
                . '</td></tr>';
        }, $resources ) )
        . '</tbody></table>'
        . foodie_settings_tablenav( 'bottom', $page_count, $row_count );

}

function foodie_settings_debug_cb() {
  echo "<p>Debug mode enabled</p>";
  echo '<button type="button" class="foodie-reset-database button">Reset Database</button>';
}

add_action( 'admin_init', 'foodie_init_settings' );

function foodie_options_page() {
  add_options_page(
    __('Foodie Settings', 'foodie'),
    __('Foodie', 'foodie'),
    'manage_options',
    'foodie',
    'foodie_options_page_html' );
}

function foodie_options_page_html() {
  if (!current_user_can('manage_options')) {
    return;
  }

  add_thickbox();

  if (isset($_GET['settings-updated'])) {
    add_settings_error( 'foodie_messages', 'foodie_message', __('Settings Saved', 'foodie'), 'updated' );
  }

  settings_errors('foodie_messages');

  echo '
<div class="wrap">
  <h2>' . esc_html( get_admin_page_title() ) . '</h2>
  <small>version ' . FOODIE_VERSION . '</small>
  <form>';

  settings_fields( 'foodie' );
  do_settings_sections( 'foodie' );
  submit_button( 'Save Settings' );

  echo '
  </form>
</div>';

  echo '<div id="foodie-options-modal" hidden></div>';

}

add_action( 'wp_ajax_foodie_reset_database', 'foodie_reset_database' );

function foodie_reset_database() {
  global $wpdb;
  foodie_log( 'foodie_reset_database()' );
  check_ajax_referer( 'foodie_nonce', '_nonce' );
  require_once plugin_dir_path( __FILE__ ) . '/hooks.php';

  $dbpfx = foodie_prefix();
  $databases = ['resources','questions','answers'];

  foreach ($databases as $db) {
    $wpdb->query("DROP TABLE IF EXISTS {$dbpfx}{$db}");
  }

  foodie_create_tables($dbpfx);
  foodie_init_db($dbpfx);

  wp_send_json( [ 'data' => 'ok' ] );
  wp_die();
}

add_action( 'wp_ajax_foodie_save_settings', 'foodie_save_settings' );

function foodie_save_settings() {
  global $wpdb;
  foodie_log( 'foodie_save_settings() $_REQUEST = ' . json_encode( $_REQUEST ) );
  check_ajax_referer( 'foodie_nonce', '_nonce' );

  $dbpfx = foodie_prefix();
  $table = "{$dbpfx}questions";
  $questions = json_decode(
    empty($_REQUEST['questions'])
    ? '[]'
    : stripslashes( $_REQUEST['questions'] ) );

  if (isset($_REQUEST['foodie_gmaps_api_key'])) {
    update_option( 'foodie_gmaps_api_key', $_REQUEST['foodie_gmaps_api_key'] );
  }

  if (isset($_REQUEST['foodie_gmaps_lat'])) {
    update_option( 'foodie_gmaps_lat', $_REQUEST['foodie_gmaps_lat'] );
  }

  if (isset($_REQUEST['foodie_gmaps_lng'])) {
    update_option( 'foodie_gmaps_lng', $_REQUEST['foodie_gmaps_lng'] );
  }

  if (isset($_REQUEST['foodie_gmaps_zoom'])) {
    update_option( 'foodie_gmaps_zoom', $_REQUEST['foodie_gmaps_zoom'] );
  }

  if (isset($_REQUEST['foodie_edit_resource_page'])) {
    update_option( 'foodie_edit_resource_page', $_REQUEST['foodie_edit_resource_page'] );
  }

  if (isset($_REQUEST['foodie_captcha_site_key'])) {
    update_option( 'foodie_captcha_site_key', $_REQUEST['foodie_captcha_site_key'] );
  }

  if (isset($_REQUEST['foodie_captcha_secret_key'])) {
    update_option( 'foodie_captcha_secret_key', $_REQUEST['foodie_captcha_secret_key'] );
  }

  foreach ($questions ?: [] as $row) {

    $data = [ 'question' => $row->question,
              'type' => $row->type,
              'required' => $row->required,
              'options' => isset($row->options) ? json_encode( $row->options ) : null ];

    if ($row->id === 'new') {
      foodie_log( "insert into $table: " . json_encode( $data ) );
      $wpdb->insert( $table, $data );
    } else {
      foodie_log( "update $table: " . json_encode( $data ) );
      $wpdb->update( $table, $data, [ 'question_id' => $row->id ] );
    }

  }

  wp_send_json([ 'data' => 'ok' ]);
  wp_die();
}

add_action( 'wp_ajax_foodie_search_resource', 'foodie_search_resource' );

function foodie_search_resource() {
  foodie_log( 'foodie_search_resource() $_REQUEST = ' . json_encode( $_REQUEST ) );
  check_ajax_referer( 'foodie_nonce', '_nonce' );

  $resources = foodie_get_resources([
      'query' => $_REQUEST['query'],
      'page' => $_REQUEST['page']
  ]);

  wp_send_json([ 'data' => $resources ]);
  wp_die();
}

add_action( 'wp_ajax_foodie_upload_resources', 'foodie_upload_resources' );

function foodie_upload_resources() {
  foodie_log( 'foodie_upload_resources() $_REQUEST = ' . json_encode( $_REQUEST ) );
  check_ajax_referer( 'foodie_nonce', '_nonce' );

  $file = $_FILES['file'];

  if (!empty($file['tmp_name']) && empty($file['error'])) {

      foodie_save_file( $file['tmp_name'] );

      wp_send_json([ 'data' => 'ok' ]);

  } else {

      foodie_log( 'foodie_upload_resources() $file = ' . json_encode( $file ) );
      switch ($file['error'] ?? -1) {
      case UPLOAD_ERR_INI_SIZE:
      case UPLOAD_ERR_FORM_SIZE:
          $error = 'The uploaded file is too large';
          break;
      case UPLOAD_ERR_PARTIAL:
          $error = 'The uploaded file was only partially uploaded';
          break;
      default:
          $error = 'Failed to upload file';
          break;
      }

      wp_send_json([ 'error' => $error ]);
  }

  wp_die();

}

function foodie_save_file ($file_path) {

    global $wpdb;
    $dbpfx = foodie_prefix();

    $columns = ['resource_id','title','lat','lng','address','phone','url',
                'hours','description','place_id','contact_name',
                'contact_phone','contact_email','contact_info','created'];

    $h = fopen( $file_path, 'r' );

    $header = fgetcsv( $h );
    $question_header = array_diff( $header, $columns );

    $header = array_flip( $header );

    $questions = json_decode( $wpdb->get_var( "
SELECT
  JSON_OBJECTAGG(
    question,
    JSON_OBJECT( 'question_id', question_id, 'type', type ) )
FROM {$dbpfx}questions
" ) );


    try {

        while (($row = fgetcsv( $h ))) {
            if (!empty($row)) {

                $resource = array_reduce(
                    $columns,
                    function ($res, $col) use ($row, $header) {
                        $res[ $col ] = $row[ $header[ $col ] ];
                        return $res;
                    },
                    [] );

                $question_param = array_reduce(
                    $question_header,
                    function ($param, $col) use ($row, $header, $questions) {
                        $q = $questions->{$col};
                        if (($qid = $q->id ?? null)) {
                            $param[ $qid ] = $row[ $header[ $col ] ];
                        }
                        return $param;
                    },
                    [] );

                $resource_id = $resource['resource_id'] ?? null;
                unset( $resource['resource_id'] );

                foodie_log( '$resource ' . json_encode( $resource ) );
                foodie_log( '$question_param ' . json_encode( $question_param ) );

                if (empty( $resource_id )) {
                    foodie_insert_resource( $resource, $question_param );
                } else {
                    foodie_update_resource( $resource_id, $resource, $question_param );
                }

            }
        }

    } finally {

        fclose( $h );

    }
}

add_action( 'admin_menu', 'foodie_options_page' );

add_action( 'admin_enqueue_scripts', function($hook) {

  if ($hook != 'settings_page_foodie') {
    return;
  }

  foodie_log( 'admin_enqueue_scripts $hook = ' . $hook );
  foodie_register( 'script', 'foodie_lib', 'js/foodie_lib.js' );
  wp_enqueue_script( 'foodie_lib' );

  wp_localize_script( 'foodie_lib', 'foodie_lib_data', [
      'api_url' => get_site_url( null, 'wp-json' ),
      'lat' => get_option('foodie_gmaps_lat'),
      'lng' => get_option('foodie_gmaps_lng'),
      'zoom' => get_option('foodie_gmaps_zoom') ] );


  $key = get_option('foodie_gmaps_api_key');
  wp_register_script(
      'foodie_gmaps',
      "https://maps.googleapis.com/maps/api/js?key=$key&callback=foodieInitMap&libraries=places",
      [],
      null,
      true );

  wp_enqueue_script( 'foodie_gmaps' );

  wp_enqueue_script( 'foodie_settings.js', plugins_url( 'js/foodie_settings.js', __FILE__ ), [], filemtime( plugin_dir_path(__FILE__) . 'js/foodie_settings.js' ) );

  wp_localize_script( 'foodie_settings.js', 'foodie_settings_data', [
    'plugin_url' => plugins_url( '', __FILE__ ),
    'api_url' => get_site_url( null, 'wp-json' ),
    'edit_resource_page' => get_option('foodie_edit_resource_page'),
    'nonce' => wp_create_nonce( 'foodie_nonce' )
  ] );

} );
