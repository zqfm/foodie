<?php
/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/

if (!defined('WPINC')) { die; }

add_action( 'rest_api_init', function() {

    function _foodie_get_resource ($data) {
        foodie_log( '_foodie_get_resource( ' . json_encode( $data->get_params() ) . ' )' );

        return [ 'data' => empty($data['id'])
                 ? foodie_get_resources()
                 : foodie_get_resource_by_id( $data['id'] ) ];
    }

    function _foodie_verify_captcha ($data) {

        if (($site_key = get_option('foodie_captcha_site_key'))) {
            $secret = get_option('foodie_captcha_secret_key');
            $response = $data->get_param('g-recaptcha-response');
            $verified = file_get_contents(
                'https://www.google.com/recaptcha/api/siteverify',
                false,
                stream_context_create([
                    'http' =>
                    [ 'method' => 'POST',
                      'header' => 'Content-Type: application/x-www-form-urlencoded',
                      'content' => http_build_query([
                          'secret' => $secret,
                          'response' => $response
                      ]) ] ]) );

            foodie_log( '$verified ' . $verified );

            return json_decode( $verified );

        } else {
            return (object)[ 'success' => true ];
        }

    }

    function _foodie_post_resource ($data) {
        foodie_log( '_foodie_post_resource( ' . json_encode( $data->get_params() ) . ' )' );

        $verified = _foodie_verify_captcha($data);
        if (!$verified->success) {
            return [ 'error' => $verified->{'error-codes'} ];
        }

        foodie_insert_resource(
            [ 'title' => $data->get_param('resource-name'),
              'address' => ($data->get_param('resource-address-street') . "\n"
                            . $data->get_param('resource-address-city') . "\n"
                            . $data->get_param('resource-address-state') . "\n"
                            . $data->get_param('resource-address-zip')),
              'lat' => $data->get_param('resource-lat'),
              'lng' => $data->get_param('resource-lng'),
              'gmaps_place_id' => $data->get_param('resource-place-id'),
              'phone' => preg_replace( '/[^0-9]/', '', $data->get_param('resource-phone') ),
              'url' => $data->get_param('resource-url'),
              'contact_name' => $data->get_param('contact-name'),
              'contact_phone' => preg_replace( '/[^0-9]/', '', $data->get_param('contact-phone') ),
              'contact_email' => $data->get_param('contact-email'),
              'contact_info' => $data->get_param('contact-info') ],
            $data->get_param('question') );

        return [ 'data' => $data->get_params() ];
    }

    function _foodie_patch_resource ($data) {
        foodie_log( '_foodie_patch_resource( ' . json_encode( $data->get_params() ) . ' )' );

        $verified = _foodie_verify_captcha($data);
        if (!$verified->success) {
            return [ 'error' => $verified->{'error-codes'} ];
        }

        $ret = foodie_update_resource(
            $data['id'],
            [ 'title' => $data->get_param('resource-name'),
              'address' => ($data->get_param('resource-address-street') . "\n"
                            . $data->get_param('resource-address-city') . "\n"
                            . $data->get_param('resource-address-state') . "\n"
                            . $data->get_param('resource-address-zip')),
              'lat' => $data->get_param('resource-lat'),
              'lng' => $data->get_param('resource-lng'),
              'gmaps_place_id' => $data->get_param('resource-place-id'),
              'phone' => preg_replace( '/[^0-9]/', '', $data->get_param('resource-phone') ),
              'url' => $data->get_param('resource-url'),
              'contact_name' => $data->get_param('contact-name'),
              'contact_phone' => preg_replace( '/[^0-9]/', '', $data->get_param('contact-phone') ),
              'contact_email' => $data->get_param('contact-email'),
              'contact_info' => $data->get_param('contact-info') ],
            $data->get_param('question') );

        return [ 'data' => $data->get_params() ];
    }

    function _foodie_delete_resource ($data) {
        foodie_log( '_foodie_delete_resource( ' . json_encode( $data->get_params() ) . ' )' );
        return [ 'data' => foodie_delete_resource( $data['id'] ) ];
    }

    register_rest_route( 'foodie/v1', '/resource/', [
        'methods' => 'GET',
        'callback' => '_foodie_get_resource'
    ] );

    register_rest_route( 'foodie/v1', '/resource/(?P<id>\d+)', [
        'methods' => 'GET',
        'callback' => '_foodie_get_resource'
    ] );

    register_rest_route( 'foodie/v1', '/resource/', [
        'methods' => 'POST',
        'callback' => '_foodie_post_resource'
    ] );

    register_rest_route( 'foodie/v1', '/resource/(?P<id>\d+)', [
        'methods' => 'PATCH',
        'callback' => '_foodie_patch_resource'
    ] );

    register_rest_route( 'foodie/v1', '/resource/(?P<id>\d+)', [
        'methods' => 'DELETE',
        'callback' => '_foodie_delete_resource'
    ] );

} );
