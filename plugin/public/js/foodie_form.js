/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/
(function (self) {

  var foodie = self.foodie;
  var map, places;

  // per https://medium.com/@almestaadmicadiab/how-to-parse-google-maps-address-components-geocoder-response-774d1f3375d
  function getAddressObject (components) {

    var shouldBeComponent = {
      number: ['street_number'],
      street: ['street_address', 'route'],
      city: [
        'locality',
        'sublocality',
        'sublocality_level_1',
        'sublocality_level_2',
        'sublocality_level_3',
        'sublocality_level_4'
      ],
      region: [
        'administrative_area_level_1',
        'administrative_area_level_2',
        'administrative_area_level_3',
        'administrative_area_level_4',
        'administrative_area_level_5'
      ],
      postal_code: ['postal_code'],
      country: ['country']
    };

    return (components || []).reduce( function (address, component) {
      for (var shouldBe in shouldBeComponent) {
        if (shouldBeComponent[shouldBe].includes(component.types[0])) {
          address[shouldBe] = ['country','region'].includes(shouldBe)
            ? component.short_name
            : component.long_name;
        }
      }
      return address;
    }, {} );

  }

  self.foodieInitMap = function () {

    map = foodie.initMap('.foodie-form .foodie-map');
    places = new google.maps.places.PlacesService( map );

    console.log( 'bounds', map.getBounds() );

    var form = document.querySelector('.foodie-form');

    var searchBox = new google.maps.places.SearchBox(
      form.querySelector('input[name="resource-name"]'),
      { bounds: map.getBounds() } );

    google.maps.event.addListener( map, 'bounds_changed', function () {
      searchBox.setBounds( map.getBounds() );
    } );

    var placeId = (form.querySelector('input[name="resource-place-id"]') || {}).value;

    if (placeId) {
      places.getDetails(
        { placeId: placeId },
        function (place, status) {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            var marker = new google.maps.Marker({
              map: map,
              position: place.geometry.location
            });
            map.setCenter( marker.getPosition() );
          }
        } );
    }

    var markers = [];
    searchBox.addListener( 'places_changed', function () {
      var results = searchBox.getPlaces();
      console.log( 'places_changed', results );

      markers.forEach( function (marker) {
        marker.setMap( null );
      } );
      markers = [];

      var bounds = new google.maps.LatLngBounds();
      results.forEach( function (result) {

        if (!result.geometry) return;

        markers.push( new google.maps.Marker({
          map: map,
          icon: {
            url: result.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          },
          title: result.name,
          position: result.geometry.location
        }) );

        if (result.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(result.geometry.viewport);
        } else {
          bounds.extend(result.geometry.location);
        }

        form.querySelector('input[name="resource-place-id"]')
          .value = result.place_id;

        form.querySelector('input[name="resource-lat"]')
          .value = result.geometry.location.lat();

        form.querySelector('input[name="resource-lng"]')
          .value = result.geometry.location.lng();

        if (result.name) {
          form.querySelector('input[name="resource-name"]')
            .value = result.name;
        }

        if (result.address_components) {

          var address = getAddressObject( result.address_components );

          form.querySelector('input[name="resource-address-street"]')
            .value = (((address.number || '') + ' ' + (address.street || '')).trim()
                      || result.formatted_address.split(',')[0]);
          form.querySelector('input[name="resource-address-city"]')
            .value = address.city || '';
          form.querySelector('input[name="resource-address-state"]')
            .value = address.region || '';
          form.querySelector('input[name="resource-address-zip"]')
            .value = address.postal_code || '';

        }

        form.querySelector('input[name="resource-phone"]')
          .value = result.formatted_phone_number || '';
        form.querySelector('input[name="resource-url"]')
          .value = result.website || '';

      } );
      map.fitBounds( bounds );

    } );

  };

  self.foodie_captcha_cb = function (response) {
    jQuery('.foodie-form button[type="submit"]')
      .prop('disabled',!response)
      .toggleClass('disabled',!response);
  };

  self.foodie_captcha_expired_cb = function () {
    jQuery('.foodie-form button[type="submit"]')
      .prop('disabled',true)
      .toggleClass('disabled',true);
  };

  jQuery(document).ready( function ($) {

    if (document.querySelector('input[name="resource-id"]')) {
      $('input[name$="-phone"]').val( function (_, val) {
        return foodie.formatPhone( val );
      } );

    }

    if ($('.g-recaptcha').length > 0) {
      $('.foodie-form button[type="submit"]')
        .prop('disabled',true)
        .toggleClass('disabled',true);
    }

    $('input[name="resource-name"]')
      .on( 'blur', function (e) {

        /*
        if (places) {

          places.textSearch(
            { query: e.currentTarget.value,
              location: map.getCenter() },
            function (result, status) {
              console.log( 'result', result, 'status', status );
            } );

        }
        */

      } );

    $('.foodie-form')
      .on( 'submit', function (e) {
        e.preventDefault();
        var submit_button = e.currentTarget.querySelector('button[type="submit"]');
        submit_button.disabled = true;
        submit_button.innerHTML = '<img src="/wp-admin/images/loading.gif"> Saving&hellip;';
        var resource_id = e.currentTarget.querySelector('input[name="resource-id"]').value;
        var captcha = typeof grecaptcha === 'undefined' ? '' : grecaptcha.getResponse();
        $.ajax(
          { url: foodie_form_data.api_url + '/foodie/v1/resource/' + resource_id,
            method: resource_id ? 'PATCH' : 'POST',
            data: $( e.currentTarget ).serialize() } )
          .done( function (response) {
            if (response.data) {
              submit_button.textContent = 'Saved!';
            } else {
              submit_button.textContent = 'Error Saving - Try Again';
            }
          } )
          .fail( function () {
            submit_button.textContent = 'Error Saving - Try Again';
          } )
          .always( function () {
            submit_button.disabled = false;
          } );
      } );

  } );

})(window);
