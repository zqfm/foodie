/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/
(function (self) {

  var foodie = self.foodie;
  var map, resources = [];

  self.foodieInitMap = function () {
    map = foodie.initMap('.foodie-map');
  };

  jQuery(document).ready( function ($) {

    var types = $('.foodie-map').data('types');

    $.getJSON( foodie_map_data.api_url + '/foodie/v1/resource/' )
      .done( function (data) {

        if (data.error) {

        } else {

          (data.data || []).forEach(
            function (res) {

              var restype = (res.answers || []).find( function (answer) { return answer.question === 'Resource Type'; } ) || {};

              if (res.lat && res.lng) {
                var marker = new google.maps.Marker({
                  map: map,
                  animation: google.maps.Animation.DROP,
                  position: { lat: parseFloat( res.lat ),
                              lng: parseFloat( res.lng ) },
                  title: res.title,
                  icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 7,
                    fillColor: types[ restype.answer ] || 'red',
                    fillOpacity: 0.4,
                    strokeColor: types[ restype.answer ] || 'red',
                    strokeWeight: 2
                  }
                });

                res.answers = res.answers || [];
                res.answersearch = res.answers.map( function(answer) { return answer.answer; } ).join('\t');
                marker.addListener( 'click', onClick( res, map, marker ) );
                res.marker = marker;
                resources.push( res );
              }

            } );

        }

      } );

    $('.foodie-form input[name="query"]')
      .on( 'input', function (e) {

        var query = e.currentTarget.value;
        var rx = new RegExp(query, 'i');

        resources.forEach( function (res) {

          res.marker.setVisible(
            !query
              || rx.test( res.title )
              || rx.test( res.answersearch ) );

        } );

      } );

    /*
    initGeoLocation( function (pos) {
      if (pos) {
        map.panTo( pos );
        map.setZoom( 12 );
      }
    } );
    */

    var infoWindows = {};
    function onClick (res, map, marker) {
      return function () {
        if (!infoWindows[ res.resource_id ]) {

          infoWindows[ res.resource_id ] = new google.maps.InfoWindow({
            content: renderInfoWindow( res )
          });

        }

        infoWindows[ res.resource_id ].open( map, marker );
      };
    }

    function renderInfoWindow (res) {

      var body = $('<div>');
      var content = $('<div>', { class: 'content' })
          .append( $('<h1>', { class: res.color }).text( res.title ),
                   body );


      if (res.address && res.address.trim()) {

        $('<address>')
          .text( res.address.trim() )
          .appendTo( body );

      }

      if (res.desc && res.desc.trim()) {

        $('p', { class: 'desc' })
          .text( res.desc.trim() )
          .appendTo( body );

      }

      var phone = res.phone && res.phone.trim();
      if (phone) {

        $('<div>', { class: 'phone' })
          .append( $('<label>').text( 'Phone' ),
                   ': ',
                   $('<a>', { href: 'tel:' + (phone.length === 10 ? '+1' + phone : phone) })
                   .text( foodie.formatPhone( phone ) ) )
          .appendTo( body );

      }

      var url = res.url && res.url.trim();
      if (url) {
        $('<div>', { class: 'url' })
          .append( $('<label>').text( 'Website' ),
                   ': ',
                   $('<a>', { target: '_blank',
                              href: (/^http/.test( url )
                                     ? url
                                     : 'http://' + url) })
                   .text( url ) )
          .appendTo( body );
      }

      var answers = res.answers;
      if (answers) {
        answers.forEach( function (answer) {

          if (answer.answer) {
            $('<div>')
              .append( $('<label>').text( answer.question ),
                       ': ',
                       $('<span>').text( answer.answer ) )
              .appendTo( body );
          }

        } );
      }

      body.append( $('<small>').append(
        $('<a>', { href: foodie_map_data.edit_resource_page + '?foodie-resource=' + encodeURIComponent( res.resource_id ) })
          .text( 'Edit Resource' ) ) );

      return content[0].outerHTML;

    }


  } );

})(window);
