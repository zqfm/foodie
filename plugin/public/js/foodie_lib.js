/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/
(function (self) {

  var foodie = self.foodie = self.foodie || {};

  foodie.initMap = function (selector) {

    console.log( 'foodie.initMap( ' + selector + ' )' );

    var map = new google.maps.Map(

      document.querySelector(selector),

      { center: { lat: parseFloat( foodie_lib_data.lat ) || 39.8290,
                  lng: parseFloat( foodie_lib_data.lng ) || -84.8909 },
        zoom: parseInt( foodie_lib_data.zoom, 10 ) || 12,
        clickableIcons: false,
        mapTypeControl: false,
        styles: [{ featureType: 'poi', // turn off points of interest
                   elementType: 'labels',
                   stylers: [{ visibility: 'off' }] }],
        controls: renderControls() } );

    return map;

  };

  function renderControls () {
    return [];
  }

  foodie.initGeolocation = function (callback) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        function (pos) {
          callback({
            lat: pos.coords.latitude,
            lng: pos.coords.longitude
          });
        },
        function () {
          callback( null );
        } );
    } else {
      callback( null );
    }
  };

  foodie.formatPhone = function (phone) {
    var parts = /(\d{3})(\d{3})(\d{4})(\d*)/.exec( phone );
    if (parts) {
      return '(' + parts[1] + ') ' + parts[2] + '-' + parts[3] + (parts[4] ? ' +' + parts[4] : '');
    } else {
      return '';
    }
  };

})(window);
