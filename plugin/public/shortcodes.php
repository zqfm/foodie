<?php
/*
Foodie is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Foodie is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foodie. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html
*/

if (!defined('WPINC')) { die; }

(function () {

    function foodie_shortcodes_init () {

        /**
         * render the map
         **/
        function foodie_map_shortcode ($atts, $content, $tag) {

            global $wpdb;

            wp_enqueue_style( 'foodie_css' );

            wp_enqueue_script( 'foodie_promise' );
            wp_enqueue_script( 'foodie_fetch' );
            wp_enqueue_script( 'foodie_lib' );
            wp_enqueue_script( 'foodie_map' );
            wp_enqueue_script( 'foodie_gmaps' );

            $dbpfx = foodie_prefix();
            $json = $wpdb->get_var( "SELECT options FROM {$dbpfx}questions WHERE question_id = 1" );
            $typemap = array_reduce(
                json_decode( $json ),
                function ($acc,$opt) {
                    $acc[ $opt->type ] = $opt->color;
                    return $acc;
                },
                [] );
            $types = htmlspecialchars( json_encode( $typemap ), ENT_QUOTES );

            $width = $atts['width'] ?? '100%';
            $height = $atts['height'] ?? '400px';

            $html = <<<EOS
<div class="foodie-form">
  <label>
    <span>Search</span>
    <input name="query" class="query" type="text">
  </label>
</div>
<div class="foodie-map" style="width:$width;height:$height" data-types="$types"></div>
EOS;

            return $html;

        }

        add_shortcode( 'foodie_map', 'foodie_map_shortcode' );

        function _foodie_select ($id, $options, $value=null) {
            return '<select name="question['
                . $id
                . ']"><option>Select One</option>'
                . implode(
                    '',
                    array_map( function($option) use ($id, $value) {
                        return '<option' . ($value && $option === $value ? ' selected' : '') . '>'
                             . htmlspecialchars( $id == 1 ? $option->type : $option )
                             . '</option>';
                    }, json_decode( $options ) ) )
                . '</select>';
        }



        /**
         * render the form
         **/
        function foodie_form_shortcode ($atts, $content, $tag) {

            global $wpdb;

            $atts = array_change_key_case( $atts ?: [] );
            $captcha_key = get_option('foodie_captcha_site_key');

            foodie_log( 'foodie_form_shortcode( ' . json_encode( $atts ) . ' )' );

            wp_enqueue_style( 'foodie_css' );

            wp_enqueue_script( 'foodie_lib' );
            wp_enqueue_script( 'foodie_form' );
            wp_enqueue_script( 'foodie_gmaps' );
            if (!empty($captcha_key)) {
                wp_enqueue_script( 'foodie_captcha' );
            }

            $resource_id = intval( strcasecmp( $atts[0] ?? '', 'edit' ) === 0 ? $_GET['foodie-resource'] ?? 0 : 0 );

            if ($resource_id
                && ($resource = foodie_get_resource_by_id( $resource_id )))
            {
                $address = new stdClass;
                list( $address->street, $address->city, $address->state, $address->zip ) = explode( "\n", $resource->address );
            } else {
                $resource = $address = new stdClass;
            }

            $esc = function ($obj,$key) {
                return htmlspecialchars( $obj->{$key} ?? '', ENT_QUOTES );
            };

            $dbpfx = foodie_prefix();
            $questions = $wpdb->get_results("SELECT * FROM {$dbpfx}questions");

            return '
<form class="foodie-form">
  <p>
    <span class="required">*</span> Required fields
  </p>
  <fieldset>
    <legend>Your Contact Information</legend>
    <label>
      <span>Your Name<span class="required">*</span></span>
      <input name="contact-name" type="text" required value="' . $esc($resource,'contact_name') . '">
    </label>
    <label>
      <span>Your Phone Number<span class="required">*</span></span>
      <input name="contact-phone" type="tel" required value="' . $esc($resource,'contact_phone') . '">
    </label>
    <label>
      <span>Your Email</span>
      <input name="contact-email" type="email" value="' . $esc($resource,'contact_email') . '">
    </label>
    <label>
      <span>Additional Information</span>
      <input name="contact-info" type="text" value="' . $esc($resource,'contact_info') . '">
    </label>
  </fieldset>
  <fieldset>
    <legend>Resource Information</legend>
    <label>
      <span>Resource Name<span class="required">*</span></span>
      <input name="resource-name" type="text" required value="' . $esc($resource,'title') . '">
    </label>
    <div class="foodie-map"></div>
    <label>
      <span>Address<span class="required">*</span></span>
      <input name="resource-address-street" type="text" required value="' . $esc($address,'street') . '">
    </label>
    <label>
      <span>City<span class="required">*</span></span>
      <input name="resource-address-city" type="text" required value="' . $esc($address,'city') . '">
    </label>
    <label>
      <span>State<span class="required">*</span></span>
      <input name="resource-address-state" type="text" required value="' . $esc($address,'state') . '">
    </label>
    <label>
      <span>Zip Code<span class="required">*</span></span>
      <input name="resource-address-zip" type="text" required value="' . $esc($address,'zip') . '">
    </label>
    <label>
      <span>Phone Number<span class="required">*</span></span>
      <input name="resource-phone" type="tel" required value="' . $esc($resource,'phone') . '">
    </label>
    <label>
      <span>Website<span class="required">*</span></span>
      <input name="resource-url" type="text" required value="' . $esc($resource,'url') . '">
    </label>
    <input type="hidden" name="resource-id" value="' . $esc($resource,'resource_id') . '">
    <input type="hidden" name="resource-place-id" value="' . $esc($resource,'gmaps_place_id') . '">
    <input type="hidden" name="resource-lat" value="' . $esc($resource,'lat') . '">
    <input type="hidden" name="resource-lng" value="' . $esc($resource,'lng') . '">
  </fieldset>
  <fieldset>
    <legend>Additional Information</legend>'
            . implode( '', array_map( function($row) use ($resource) {
                foodie_log( '$row: ' . json_encode( $row ) );
                $answer = array_reduce( $resource->answers ?? [], function ($acc, $answer) use ($row) {
                    foodie_log( '$answer: ' . json_encode( $answer ) );
                    if ($acc) { return $acc; }
                    if (strcasecmp( $row->question, $answer->question ) === 0) {
                        return $answer->answer;
                    }
                }, null );
                return '<label><span>'
                    . htmlspecialchars( $row->question )
                    . ($row->required ? '<span class="required">*</span>' : '')
                    . '</span>'
                    . ($row->type === 'enum' ? _foodie_select( $row->question_id, $row->options, $answer )
                       : ($row->type === 'bool' ? '<input type="checkbox" name="question[' . $row->question_id . ']"' . ($answer ? ' checked' : '') . '>'
                          : '<input type="text" name="question[' . $row->question_id . ']" value="' . ($answer?:'') . '">'))
                    . '</label>';
            }, $questions ) ) . '
  </fieldset>'
                  . (empty($captcha_key) ? '' : '<div class="g-recaptcha" data-sitekey="' . htmlspecialchars( $captcha_key, ENT_QUOTES ) . '" data-callback="foodie_captcha_cb" data-expired-callback="foodie_captcha_expired_cb"></div>')
  .
  '<button type="submit">Submit</button>
</form>';

        }

        add_shortcode( 'foodie_form', 'foodie_form_shortcode' );

    }

    function foodie_register ($type,$name,$path,$opts=[]) {
        $fn = $type === 'style' ? 'wp_register_style' : 'wp_register_script';
        $fn( $name, plugins_url( $path, __FILE__ ), $opts['deps'] ?? [], filemtime( plugin_dir_path(__FILE__) . $path ) );
    }

    add_action( 'init', 'foodie_shortcodes_init' );

    add_action('wp_enqueue_scripts', function () {
        foodie_log( 'foodie wp_enqueue_scripts' );

        foodie_register( 'style', 'foodie_css', 'css/foodie.css' );

        /**
         * js common to foodie_map and foodie_form
         **/
        foodie_register( 'script', 'foodie_lib', 'js/foodie_lib.js' );

        wp_localize_script( 'foodie_lib', 'foodie_lib_data', [
            'api_url' => get_site_url( null, 'wp-json' ),
            'lat' => get_option('foodie_gmaps_lat'),
            'lng' => get_option('foodie_gmaps_lng'),
            'zoom' => get_option('foodie_gmaps_zoom') ] );

        /**
         * the js that handles the interactive google map
         **/
        foodie_register( 'script', 'foodie_map', 'js/foodie_map.js', [ 'deps' => ['jquery','foodie_lib'] ] );

        wp_localize_script( 'foodie_map', 'foodie_map_data', [
            'api_url' => get_site_url( null, 'wp-json' ),
            'edit_resource_page' => get_option('foodie_edit_resource_page') ] );

        /**
         * the js that handles the resource creation form
         **/
        foodie_register( 'script', 'foodie_form', 'js/foodie_form.js', [ 'deps' => ['jquery','foodie_lib'] ] );

        wp_localize_script( 'foodie_form', 'foodie_form_data', [
            'api_url' => get_site_url( null, 'wp-json' ) ] );

        /**
         * load the google maps api with the given key
         **/
        if (($key = urlencode( get_option('foodie_gmaps_api_key') ))) {
            wp_register_script(
                'foodie_gmaps',
                "https://maps.googleapis.com/maps/api/js?key=$key&callback=foodieInitMap&libraries=places",
                [],
                null,
                true );
        }

        if (get_option('foodie_captcha_site_key')) {
            wp_register_script(
                'foodie_captcha',
                'https://www.google.com/recaptcha/api.js##async##defer',
                [],
                null,
                true );

        }
    });

    /**
     * The recaptcha js should be loaded async and deferred, but wordpress
     * doesn't have built in provisions for doing that with wp_register_script
     * so we hack that in here by adding a filter to the clean_url hook
     **/
    add_filter( 'clean_url', function ($url) {

        if (strpos($url, '##async') === false) {
            // pass
        } else if (is_admin()) {
            $url = str_replace('##async', '', $url);
        } else {
            $url = str_replace('##async', '', $url)."' async='async";
        }

        if (strpos($url, '##defer') === false) {
            // pass
        } else if (is_admin()) {
            $url = str_replace('##defer', '', $url);
        } else {
            $url = str_replace('##defer', '', $url)."' defer='defer";
        }

        return $url;
    }, 11 );

})();
